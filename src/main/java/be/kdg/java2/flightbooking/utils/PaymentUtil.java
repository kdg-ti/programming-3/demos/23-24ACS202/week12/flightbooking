package be.kdg.java2.flightbooking.utils;

import be.kdg.java2.flightbooking.exceptions.InsufficientMoneyException;
import be.kdg.java2.flightbooking.exceptions.InvalidAccountException;

public class PaymentUtil {
    public static void checkAccount(String account, double amount) {
        checkAccountValidity(account);
        if (amount>1000) throw new InsufficientMoneyException("Not enough money on the account " + account);
    }

    public static void checkAccountValidity(String account) {
        if (account==null||account.length()!=18) {
            throw new InvalidAccountException("Account " + account + " does not exists");
        }
    }
}
