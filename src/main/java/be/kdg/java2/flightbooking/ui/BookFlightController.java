package be.kdg.java2.flightbooking.ui;

import be.kdg.java2.flightbooking.exceptions.DestinationNotAvailableException;
import be.kdg.java2.flightbooking.exceptions.InsufficientMoneyException;
import be.kdg.java2.flightbooking.exceptions.InvalidAccountException;
import be.kdg.java2.flightbooking.service.FlightBookingService;
import be.kdg.java2.flightbooking.ui.viewmodels.FlightBookingViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BookFlightController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private FlightBookingService flightBookingService;

    public BookFlightController(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @GetMapping({"","/index","/"})
    public String getBookFlightPage(Model model){
        model.addAttribute("destinations", flightBookingService.getDestinations());
        model.addAttribute("flightBooking",new FlightBookingViewModel());
        return "index";
    }

    @PostMapping("/bookflight")
    public String bookFlight(FlightBookingViewModel flightBookingViewModel){
        try {
            flightBookingService.bookFlight(flightBookingViewModel.getName(), flightBookingViewModel.getDestination(), flightBookingViewModel.getAccount());
        } catch (DestinationNotAvailableException e) {
            logger.error("Error booking a flight:" + e.getMessage());
            return "error";
        }
        return "redirect:index";
    }

    @ExceptionHandler({InvalidAccountException.class})
    public String handleInvalidAccountException(Exception e, Model model){
        logger.error(e.getMessage());
        model.addAttribute("error", "Invalid account");
        return "error";
    }

    @ExceptionHandler(InsufficientMoneyException.class)
    public String handleInsufficientMoneyException(Exception e, Model m){
        logger.error(e.getMessage());
        m.addAttribute("error", "Invalid account");
        return "error";//you can return other view of course....
    }
}
